/* Smoothscrolling with anchors
   ========================================================================== */
$(document).on('click', '.smoothScroll', function () {
  var target = $(this).data('linkto')

  if (target.length) {
    $('html,body').animate({
      scrollTop: $('#' + target).offset().top
    }, 1000)
  }

  return true
})
