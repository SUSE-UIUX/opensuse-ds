$(function () {
  /**
  * This check comes from the variable $sd-max from assets/stylesheets/base/variables/media-query.scss
  * If at some point we change the value of $sd-max don't forget to chage this part as well
  */
  if (window.innerWidth < 769) {
    var mainMenu = $('.main-menu')
    $('.mobile-submenu-landing').slideUp()

    /* Toggle nav on click */
    $('.landing-mobile-burger').click(function () {
      if (!mainMenu.hasClass('solid-bg')) {
        mainMenu.toggleClass('solid-bg')
      }
      mainMenu.toggleClass('js-check-top')

      $('.mobile-submenu-landing').slideToggle(100)

      /* Check if .main-menu have the class js-check-top and if scroll position is less than 100, remove class of .solid-bg */
      if (!mainMenu.hasClass('js-check-top') && $(window).scrollTop() < 100) {
        mainMenu.removeClass('solid-bg')
      }
    })
  }
})
