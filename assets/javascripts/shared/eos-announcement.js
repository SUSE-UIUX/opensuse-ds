$(function () {
  var selectLocalStorage = window.localStorage.getItem('eosTeam')

  if (selectLocalStorage !== 'true') {
    $('.js-eos-announcement').modal('show')
    window.setTimeout(function () {
      window.location.href = 'https://eosdesignsystem.com'
    }, 5000)
  } else {
    $('.js-eos-announcement').modal('hide')
  }
})
