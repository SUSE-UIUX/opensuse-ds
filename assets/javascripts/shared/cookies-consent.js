$(function () {
  /* Create Cookie controler for acceptance and setup modification. */
  cookieController()
})

function cookieController () {
  /* eslint-disable no-undef */
  var cookieController = {
    // When the user accept the cookies, we write the cookies witht he configuration.
    accept: function () {
      // Cookie we use for GTM, in order to track the user needs to accept this cookie.
      Cookies.set('acceptance', 'true', { expire: 60 })
      // Cookie we use to hide the Cookies Alert once the user accepts the cookie.
      Cookies.set('cookies-preference', 'true')
      // Cookie we use to hide the Cookies Alert once the user accepts the cookie.
      Cookies.set('acceptance-remainder', 'true')
      $('.cookies-alert').fadeOut()
    },
    trackOff: function () {
      Cookies.remove('cookies-preference')
      Cookies.remove('acceptance')
      setTimeout(function () {
        window.location.reload(true)
      }, 1000)
    }
  }

  /* Cookie we use for GTM, in order to track the user needs to accept this cookie. */
  var acceptanceCookie = Cookies.get('acceptance')
  /* Cookie we use to remeber users prefferance (track on/off) */
  var preferenceCookies = Cookies.get('cookies-preference')
  /* Cookie we use to hide the Cookies Alert once the user accepts the cookie. */
  var acceptanceReminder = Cookies.get('acceptance-remainder')
  /* If the user never clicks Accept, we show the banner (display: none by default) */
  if (!acceptanceReminder) {
    $('.cookies-alert').css('display', 'flex')
  }
  /* If acceptance Cookies is present, set the cookies-preference On. */
  if (acceptanceCookie) {
    Cookies.set('cookies-preference', 'true')
  }
  /**
  * The Default setup for cookies is off (acceptance = undefined )
  * If user accept the cookies, write te cookie and fire the GTM events.
  */
  $('.js-cookies-accept').on('click', function () {
    cookieController.accept()
  })
  /**
  * If cookies are accepted, we turn ON the cookie toggle configuration in our cookies page.
  * If the box is clicked again, we turn tracking and toggle off.
  */
  if (preferenceCookies) {
    $('.analytics-tracking').prop('checked', true)
    $('.analytics-tracking').on('click', function () {
      if (preferenceCookies) {
        cookieController.trackOff()
      }
    })
  }
  /* When status of the toggle changes from off to on, restore the cookies. */
  if (!preferenceCookies) {
    $('.analytics-tracking').on('click', function () {
      cookieController.accept()
    })
  }
}
