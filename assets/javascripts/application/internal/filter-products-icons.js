$(document).on('ready', function () {
  /* When clicking on one of the products labels in the filter */
  $('.js-filter-product-icons').on('click', function () {
    var filterName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterProductsOut(filterName)
  })
  $('.js-filter-version-icons').on('click', function () {
    var filterName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterVersionsOut(filterName)
  })
  $('.js-filter-reported-icons').on('click', function () {
    var filterName = $(this)
      .clone()
      .children()
      .remove()
      .end()
      .text()
    filterReportedOut(filterName)
  })
  countIconsProducts()
  countIconsVersion()
})

function filterProductsOut (label) {
  var $iconsContainer = $('.icons-list article')
  for (var i = 0; i < $iconsContainer.length; i++) {
    var filterLabel = $($iconsContainer[i]).find('.js-filter-label').text()
    if (filterLabel === label || label === 'All') {
      $iconsContainer[i].style.display = ''
    } else {
      $iconsContainer[i].style.display = 'none'
    }
  }
}

function filterVersionsOut (label) {
  var $iconsContainer = $('.icons-list article')
  for (var i = 0; i < $iconsContainer.length; i++) {
    var filterLabel = $($iconsContainer[i]).find('.js-filter-version-label').text()
    if (filterLabel === label || label === 'All') {
      $iconsContainer[i].style.display = ''
    } else {
      $iconsContainer[i].style.display = 'none'
    }
  }
}

function filterReportedOut (label) {
  var $iconsContainer = $('.icons-list article')
  for (var i = 0; i < $iconsContainer.length; i++) {
    var filterLabel = $($iconsContainer[i]).find('.js-filter-reported-label').text()
    if (filterLabel === label || label === 'All') {
      $iconsContainer[i].style.display = ''
    } else {
      $iconsContainer[i].style.display = 'none'
    }
  }
}

function countIconsVersion () {
  var $allProducts = $('.js-count-version-icons')
  $.each($allProducts, function (i, v) {
    var productName = $(v)
      .parent()
      .clone()
      .children()
      .remove()
      .end()
      .text()
    var $counterContainer = $(v)
    var $iconsContainer = $('.icons-list article .js-filter-version-label:contains(' + productName + ')')
    var countIcons = $iconsContainer.length
    $counterContainer.html(countIcons)
  })
}

function countIconsProducts () {
  var $allProducts = $('.js-count-product-icons')
  $.each($allProducts, function (i, v) {
    var productName = $(v)
      .parent()
      .clone()
      .children()
      .remove()
      .end()
      .text()
    var $counterContainer = $(v)
    var $iconsContainer = $('.icons-list article .js-filter-label:contains(' + productName + ')')
    var countIcons = $iconsContainer.length
    $counterContainer.html(countIcons)
  })
}
