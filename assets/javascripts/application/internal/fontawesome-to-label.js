/* Take the class of fontawesome icons used and print them inside a label */
$(document).on('ready', function () {
  $('.fa').each(function () {
    var classes = $(this).attr('class')
    $(this).parent().append('<span class=\'label label-default\'>' + classes + '</span>')
  })
})
