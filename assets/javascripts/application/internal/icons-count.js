/**
 * Count how many icons are displayed on a page
 *
 * Example of usage:
 *
 * p
 *   | Total icons:
 *   span.js-count-icons
 *
 * .icons-list
 *   article
 *     ...
 *
 * the script will count the number of `article` under .icon-list and place the number in `.js-count-icons`
 */

$(function () {
  var $counterContainer = $('.js-count-icons')
  var $iconsContainer = $('.icons-list article')
  var countIcons = $iconsContainer.length
  $counterContainer.html(countIcons)
})
