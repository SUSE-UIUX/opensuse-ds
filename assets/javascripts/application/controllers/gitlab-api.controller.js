$(function () {
  renderChangelogData()
  removeChangelogFromPage()
  disableOrEnableNotification()
})

function renderChangelogData () {
  gitlabAPICall(function (result) { // eslint-disable-line no-undef
    var converter = new showdown.Converter() // eslint-disable-line no-undef

    var data = result

    /* Send last release tag so we can compare it */
    checkIfNewRelease(data[0].name)

    /* Removed the last item of the array, tag 0.0.0 since it's not available and returns null. */
    data.pop()

    /* RegEx to get the date without hour */
    var regEx = /[0-9]*-[0-9]*-[0-9]*/gm

    for (var i = 0; i < data.length; i++) {
      var createdDate = data[i].commit.created_at.match(regEx)

      try {
        $('.changelog').append('<div class="changelog-element"> <h5>' + data[i].name + '</h5> <span class="changelog-date"> Released: ' + createdDate + ' </span> ' + converter.makeHtml(data[i].release.description) + ' </div>')
      } catch (err) {
        console.error('Ups!: ' + err)
      }
    }
  })
}

/* Check if a new release is out
  ========================================================================== */
function checkIfNewRelease (data) {
  var localValue = window.localStorage.getItem('latestVersion')

  /* If ther's no latestVersion, set the localvariable */
  if (!localValue || localValue !== data) {
    /* Set the latest version tag as local variable */
    window.localStorage.setItem('latestVersion', data)
    /* Set the userChecked as false as default, so only change on click */
    window.localStorage.setItem('userCheckedVersion', false)
    /* Create a 10 days cookie to keep track from when we delete the alert box */
    Cookies.set('displayNewRelease', true, { expires: 10 }) // eslint-disable-line no-undef
  }
}

/* Remove the notification box when the cookie expire
  ========================================================================== */
function removeChangelogFromPage () {
  var cookie = Cookies.get('displayNewRelease')// eslint-disable-line no-undef
  /* If the cookie displayNewRelease is not present, remove the notification panel */
  if (!cookie) {
    $('.js-changelog').remove()
  }
}

/* Check the localstorage status to toggle inactive class
  ========================================================================== */
function disableOrEnableNotification () {
  var wasClicked = window.localStorage.getItem('userCheckedVersion')

  $('.js-changelog').on('click', function () {
    if (wasClicked === 'false') {
      window.localStorage.setItem('userCheckedVersion', true)
    }
  })

  if (wasClicked === 'false') {
    $('.js-changelog').toggleClass('changelog-box-inactive')
  }
}
