/* ==========================================================================
  Icons version and links
  ========================================================================== */

/* Prepare local variables that will be reused in different methods */
var $iconsContainer, iconDisplayTemplate

$(function () {
  $('.js-eos-icons-notification').hide()
  // Prepare icons containers
  $iconsContainer = $('.js-eos-icons-list')
  iconDisplayTemplate = $('.icon-display').clone(true)
  $('.icon-display').remove()

  // Initiate the full collection of icons
  getFullIconsCollection()

  /* Better search response and API call
  ========================================================================== */
  var typingTimer
  var doneTypingInterval = 800

  /* on keyup, start the countdown */
  $('.js-eos-icon-filter').on('keyup', function () {
    $('.js-eos-icons-notification').hide()
    clearTimeout(typingTimer)
    typingTimer = setTimeout(doneTyping, doneTypingInterval)
  })

  /* on keydown, clear the countdown */
  $('.js-eos-icon-filter').on('keydown', function () {
    clearTimeout(typingTimer)
  })

  /* once typing is done */
  function doneTyping () {
    var query = $('.js-eos-icon-filter').val()
    onSearch(query)
  }
})

function onSearch (q) {
  $('.icon-display').remove()
  if (!q) {
    // If the search came up emtpy, restore all icons
    getFullIconsCollection()
  } else {
    // If there is a search query, consume the search service
    getFilteredIconsCollection(q)
  }
}

function getFilteredIconsCollection (q) {
  searchIconsService(q, function (searchResult) { // eslint-disable-line no-undef
    var iconsMerged = searchResult
    renderIcons(iconsMerged)
  })
}

function getFullIconsCollection () {
  getIconsCollectionService(function (dataGlyph, dataPackage) { // eslint-disable-line no-undef
    // Data manipulation from glyph-list.json
    var dataGlyphCollection = dataGlyph[0]
    var repositoryUrl = dataGlyphCollection.repositoryUrl
    linkRepositoryUrl(repositoryUrl)
    var baseClass = dataGlyphCollection.baseClass
    addClassToCode(baseClass)
    var iconsGlyphs = dataGlyphCollection.glyphs
    var iconsAnimated = dataGlyphCollection.animatedIcons
    // Merge static & animated icon objects
    Object.assign(iconsGlyphs, iconsAnimated)
    var iconsMerged = Object.assign([], iconsGlyphs, iconsAnimated)

    renderIcons(iconsMerged)

    // Data manipulation from package.json
    var dataPackageCollection = dataPackage[0]
    var version = dataPackageCollection.version
    addVersionNumber(version)
  })
}

function renderIcons (collection) {
  for (var i = 0; i < collection.length; i++) {
    var newIconDisplay = iconDisplayTemplate.clone(true)
    var iconName = typeof collection[i] === 'string' ? collection[i] : collection[i].name
    // Add icon name
    $(newIconDisplay).find('.eos-icons').text(iconName)
    $(newIconDisplay).find('.icon-name').text(iconName)

    // This hack is added to all elements although it's only used for animated icons
    $(newIconDisplay).find('.eos-icons').addClass('eos-icon-' + iconName)

    $($iconsContainer).append(newIconDisplay)
  }
  /* Attach the click event on creation */
  $('.js-eos-icons-set .js-panel-slidein-open').on('click', function () {
    var iconSelected = $(this).children('.eos-icons').text()
    toggleIconInPanel(iconSelected)
  })
}

function linkRepositoryUrl (url) {
  $('.js-eos-icons-repo-link').attr('href', url)
}

function addVersionNumber (v) {
  $('.js-eos-icons-version').text(v)
}

function addClassToCode (baseClass) {
  $('.js-eos-icons-base-class').text(baseClass)
}

function toggleIconInPanel (iconName) {
  $('.js-eos-icons-name').text(iconName)

  /* Get information for the Do and Dont and the Tags */
  getMoreInfoFromIconService(iconName, function (data) { // eslint-disable-line no-undef
    var notFoundMsg = 'No information available'
    var animatedClass = data ? data.animatedClass || null : null
    var codeExample = data ? data.codeExample || null : null
    var textDo = data ? data.do || notFoundMsg : notFoundMsg
    var textDont = data ? data.dont : notFoundMsg
    var tags = data ? data.tags || notFoundMsg : notFoundMsg
    addIconSpecificData(animatedClass, codeExample, textDo, textDont, tags)
  })
}

function addIconSpecificData (animatedClass, codeExample, textDo, textDont, tags) {
  $('.js-eos-icons-animated-class').text(animatedClass)
  $('.js-eos-icons-animated-class').removeClass('hide-ligature')
  if (animatedClass != null) {
    $('.js-eos-icons-animated-class').addClass('hide-ligature')
    $('.js-eos-icons-example-class').html(jQuery.parseHTML(codeExample))
  }
  $('.js-eos-icons-do').html(jQuery.parseHTML(textDo))
  $('.js-eos-icons-dont').html(jQuery.parseHTML(textDont))
  $('.js-hide-container').removeClass('hide')
  if (textDont.length < 5) {
    $('.js-hide-container').addClass('hide')
  }
  var tag = ''
  for (var i = 0; i < tags.length; i++) {
    tag += '<span class="label label-default">' + tags[i] + '</span>'
  }
  $('.js-eos-icons-tags').html(jQuery.parseHTML(tag))
}
