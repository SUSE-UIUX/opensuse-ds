$(function () {
  generatePdf()
})

/* use html2pdf lib to generate PDF from a div. */
function generatePdf () {
  /* Select the div that would be printed */
  var element = document.getElementById('wrap-to-print')

  /* Generate the PDF on click. */
  $('.js-generate-pdf').click(function () {
    /* eslint-disable */
    html2pdf(element)
    /* eslint-enable */
  })
}
