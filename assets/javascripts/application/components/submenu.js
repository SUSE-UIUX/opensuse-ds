$(function () {
  /* Get each nav element's width and set it as attribute to be used later */
  submenuLinksSetAttribute()
  /* Display dropdown in left/right based on windows size */
  submenuDropdownPosition()

  submenuCollapse()
  /* When window is resized, run our collapse function */
  $(window).resize(submenuCollapse)

  /* On scroll, call submenuAddShadow */
  $(document).scroll(submenuAddShadow)
})

/* ==========================================================================
  Menu collapse function
  ========================================================================== */
/* Maximum number of elements allowed in the menu  */
var submenuLimit = 7

function submenuCollapse () {
  var submenuLinks = $('.js-submenu-visible a')
  var submenuMore = $('.js-submenu-more')

  var navWidth = 0
  var submenuMoreContent = $('.js-submenu-more-list')
  var submenuWidth = $('.js-submenu-section').width()
  var submenuLinksCheck

  /* Cleanup dom when resizing */
  submenuMoreContent.html('')

  /* 'smart' responsiveness */
  submenuLinks.each(function (index) {
    // Use the width data attr we setup before
    navWidth += $(this).data('width')

    // Possible combination of elements
    var moreSearchWidth = $('.js-submenu-more').outerWidth() + $('.submenu-search').outerWidth()
    var moreUserWidth = $('.js-submenu-more').outerWidth() + $('.user-submenu').outerWidth()
    var combinedWidth = $('.js-submenu-more').outerWidth() + $('.submenu-search').outerWidth() + $('.user-submenu').outerWidth()

    // Cache conditions for readability
    var condLimit = index > submenuLimit - 1
    // Default condition (no search or user)
    var condDefault = navWidth > submenuWidth - 70
    // More dropdown & search
    var condMoreSearch = navWidth > submenuWidth - moreSearchWidth
    // More dropdown & user
    var condMoreUser = navWidth > submenuWidth - moreUserWidth
    // All dropdowns
    var condCombined = navWidth > submenuWidth - combinedWidth

    // If any of these conditions are met
    if (condLimit || condDefault || condMoreSearch || condMoreUser || condCombined) {
      // Hide the nav item
      $(this).addClass('hide')
      // Clone it, strip it from any class and move it to the dropdown
      $(this).clone().removeClass().appendTo(submenuMoreContent)
      // Show the dropdown
      submenuMore.show()
      submenuLinksCheck = true
    } else {
      // If none of these conditions are met, ensure that we show the nav items
      $(this).removeClass('hide')
    }
  })

  // If the amount of nav items is greater than 7 or check if all conditions are met, display the dropdown
  if (submenuLinks.length > submenuLimit || submenuLinksCheck) {
    submenuMore.show()
  } else {
    submenuMore.hide()
  }
}

// If the width of main-submenu is greater than 200px show right dropdown menu
function submenuDropdownPosition () {
  if ($('.js-submenu-visible').outerWidth() >= 480) {
    $('.js-submenu-more ul').removeClass('dropdown-menu-left').addClass('dropdown-menu-right')
  }
}

/* Add data-width attr to navigation elements  */
function submenuLinksSetAttribute () {
  $('.js-submenu-visible a').each(function () {
    $(this).attr('data-width', $(this).outerWidth())
  })
}

/* Add shadow to submenu element on scrop */
function submenuAddShadow () {
  if ($(window).scrollTop() > 100) {
    $('.js-submenu-section').stop().addClass('submenu-scroll')
  } else {
    $('.js-submenu-section').stop().removeClass('submenu-scroll')
  }
}
