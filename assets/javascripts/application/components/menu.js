/* Return TRUE/FALSE base on windows size.
========================================================================== */
function isSmallScreen () {
  return window.innerWidth <= 1260
}

/* ==========================================================================
  Collapse funtionality
  ========================================================================== */
function collapseSidebarOnSmallScreen () {
  if (isSmallScreen()) {
    // in small screen the menu starts collapsed
    $('.main-menu').addClass('collapsed-sidebar')
    $('.footer-content').addClass('display-collapsed')
    // return true to initiate the tooltip
    return true
  }
}

function collapseSidebarBasedOnLocalStorage () {
  if (window.localStorage.getItem('collapsedSidebar') === 'true') {
    // if saved in locastorage that collapsedSidebar=true, menu starts collapsed
    $('.main-menu').addClass('collapsed-sidebar')
    // return true to initiate the tooltip
    return true
  }
}

function toggleSidebarAndSave () {
  $('.main-menu').toggleClass('collapsed-sidebar')
  $('.footer-content').toggleClass('display-collapsed')
  toggleTooltip()
  if (!isSmallScreen()) {
    saveSidebarState()
  }
}

function autoToggleSidebar () {
  var shouldCollapse = isSmallScreen() || window.localStorage.getItem('collapsedSidebar') === 'true'
  toggleTooltip()
  $('.main-menu').toggleClass('collapsed-sidebar', shouldCollapse)
  $('.footer-content').toggleClass('display-collapsed', shouldCollapse)
}

function saveSidebarState () {
  window.localStorage.setItem('collapsedSidebar', $('.main-menu').hasClass('collapsed-sidebar'))
}

// Togle on resize and click.
$(window).resize(autoToggleSidebar)
$(document).on('click', '.js-sidebar-toggle', toggleSidebarAndSave)

// When the page is done loading, check if we should toggle the menu based on width or localstorage
$(document).ready(function () {
  // Always display content-footer if showSidebar === false
  if (window.localStorage.getItem('collapsedSidebar') === 'true') {
    $('.footer-content').addClass('display-collapsed')
  }
  // if either the screen is small or it is saved in localstorage to keep the menu open
  // then we Initiate the tooltip
  if (collapseSidebarOnSmallScreen() || collapseSidebarBasedOnLocalStorage()) {
    toggleTooltip()
  }
})

// Initiate or destroy the tooltip according to the state of the collapsible menu
function toggleTooltip () {
  // if the menu is not collapsed (meaning, it is open) we dont need to show the tooltips
  if ($('.collapsed-sidebar').length === 0) {
    $('.mm-navigation-container li').tooltip('destroy')
  } else {
    // if the menu is collapsed, we initiate de tooltip
    $('.mm-navigation-container li').tooltip()
  }
}
