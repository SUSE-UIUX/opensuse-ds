$(document).ready(function () {
  showMoreResources()

  $(window).resize(function () {
    showMoreResources()
  })
})

function showMoreResources () {
  // Toggle between 3 or 2 elements to display based on windows size.
  var showManyElements = window.innerWidth > 1090 ? 3 : 2

  $('.resource-content').slice(0, showManyElements).show()
  $('.load-more').on('click', function (e) {
    e.preventDefault()
    $('.resource-content:hidden').slice(0, showManyElements * 2).slideDown()
    if ($('.resource-content:hidden').length === 0) {
      $('.load-more').fadeOut('slow')
    }
  })
}
