/* ==========================================================================
   Feedback tool with screenshot attachments
   ========================================================================== */

var attachment
/* Set the required message length  */
var messageMinAllowed = 50

$(function () {
  /* Initial status feedback elements empty */
  resetFeedback()
  $('.message-length').text(messageMinAllowed + ' characters required')

  /* Feedback open/close
  ========================================================================== */
  $('.js-feedback-tool-open').click(function () {
    $('.js-feedback-tool').toggle()
    /* Focus on the textarea once we open */
    $('.js-feedback-tool-message').focus()
  })
  $('.js-feedback-tool-close').click(function () {
    $('.js-feedback-tool').hide()
  })

  /* Close feedback on ESC */
  $(document).keydown(function (e) {
    // if keypressed is ESC (keycode 27), hide panel-slidein
    if (e.keyCode === 27) {
      if ($('.js-feedback-tool').is(':visible')) {
        $('.js-feedback-tool').toggle()
      }
    }
  })

  /* Change Send button status and message length.
  ========================================================================== */
  $('.js-feedback-tool-message').keydown(function () {
    var sendBtn = $('.js-feedback-tool-send')

    /* Show message length */
    messageMinAllowed - $(this).val().length <= 0 ? $('.message-length').text('0 characters required') : $('.message-length').text(messageMinAllowed - $(this).val().length + ' characters required')

    $(this).val().length >= messageMinAllowed ? sendBtn.removeClass('disabled') : sendBtn.addClass('disabled')
  })

  /* Just to toggle the screenshoot area */
  $('.js-feedback-tool-screenshoot-trigger').click(function () {
    $('.js-screenshot-name').toggle(this.checked)
  })

  /* Send form to back-end
  ========================================================================== */
  $('.js-feedback-tool-send').click(function (e) {
    if ($(this).hasClass('disabled')) {
      return false
    } else {
      validateForm()
    }
  })

  /* Finish the process once we click Finish in the last page.
  ========================================================================== */
  $('.js-feedback-tool-finish').click(function () {
    feedbackFormActions.finishAction()
  })
})

/* Main controller for sendForm, showSuccess page and finishAction
========================================================================== */
var feedbackFormActions = {
  showSuccess: function () {
    $('.js-feedback-tool-card-form').hide()
    $('.js-feedback-tool-card-success').show()
  },
  finishAction: function () {
    $('.js-feedback-tool-card-success').animate({
      opacity: 0.25,
      left: '+=50',
      height: 'toggle'
    }, 500, function () {
      // reset feedback tool so it can be re-opened
      resetFeedback()
    })
  },
  showError: function () {
    $('.feedback-tool-form p').addClass('alert alert-danger').text('There was an error on our side and we couldn\'t send the feedback. Please contact us at eos@suse.de')
  }
}

function sendForm (data) {
  fetch('/api/feedback', { // eslint-disable-line no-undef
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(function (res) {
    if (res.status === 200) {
      feedbackFormActions.showSuccess()
    }
    if (res.status === 500) {
      feedbackFormActions.showError()
    }
  })
}

function validateForm () {
  var formMessage = $('.js-feedback-tool-message').val()
  var dataParsed = parseFormContent(formMessage)
  /* We send the form and move to next page ONLY if msg length is larger than messageMinAllowed */
  if (dataParsed.message !== null) {
    if (dataParsed.message.length > messageMinAllowed) {
      sendForm(dataParsed)
      return true
    }
  } else {
    return false
  }
}

function resetFeedback () {
  $('.js-feedback-tool').hide()
  $('.js-feedback-tool-card-form').show()
  $('.js-feedback-tool-card-success').removeAttr('style').hide()
  $('.js-feedback-tool-message').val('')
  $('.js-screenshot-name').html('').hide()
  $('.js-feedback-tool-screenshoot-trigger').prop('checked', false)
}

/* Data that will be processed in the backend and sent via email */
function parseFormContent (msg) {
  var data = {
    message: msg || null,
    img: $('.js-feedback-tool-screenshoot-trigger').is(':checked') ? attachment : null
  }
  return data
}

/* SCREENSHOT: creates a screenshot as soon as the feedback tool is opened
========================================================================== */

$(function () {
  $('.js-screenshoot').click(function () {
    takeScreenShoot()
  })
})

/* Screenshoot functionality
  ========================================================================== */

function takeScreenShoot () {
  html2canvas(document.body, {scale: 0.3, useCORS: true, allowTaint: true}) // eslint-disable-line no-undef
    .then(function (canvas) {
      /* For implementation, make sure you send this base64 as attachment */
      var base64image = canvas.toDataURL('image/png')
      attachment = base64image
    })
    .then(function () {
      /* Define where you want to attach the 'file' name */
      var whereToAttachName = $('.js-screenshot-name')
      addAttachmentName(whereToAttachName)
    })
}

/* Generate attachment name based on window location and date.
========================================================================== */
function addAttachmentName (where) {
  var attachName = window.location.pathname.replace('/', '')

  /* Get new date to append to the window location */
  var currentDate = new Date()
  var day = currentDate.getDate()
  var month = currentDate.getMonth() + 1
  var year = currentDate.getFullYear()
  // clean it first
  where.html('')
  // append the new text
  where.append(attachName + '-' + day + '-' + month + '-' + year + '.jpg')
}
