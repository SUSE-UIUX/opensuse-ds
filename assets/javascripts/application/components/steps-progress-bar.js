$(document).ready(function () {
  var pageUrl = window.location.pathname
  pageUrl = pageUrl.split('/')
  pageUrl = pageUrl[ pageUrl.length - 1 ]

  $('.steps-progress-bar a').each(function () {
    var href = $(this).attr('href')

    if (pageUrl === href) {
      $(this).closest('li').addClass('selected')
    }
  })
})
