$(function () {
  $('.back-to-top').on('click', backToTop)
})

$(document).scroll(toggleBackToTopButton)

function backToTop () {
  $('html, body').animate({scrollTop: 0}, 500)
}

function toggleBackToTopButton () {
  var position = $(this).scrollTop()
  if (position > 800) {
    $('.back-to-top').stop().fadeIn().css('visibility', 'visible')
  } else {
    $('.back-to-top').stop().fadeOut().css('visibility', 'hidden')
  }
}
