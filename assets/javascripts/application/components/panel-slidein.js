$(document).ready(function () {
  $('.js-panel-slidein-open').on('click', panelSlideinVisible)
  function panelSlideinVisible () {
    $('.panel-slidein').addClass('visible')
    closeWithEsc()
  }
  $('.panel-slidein-close').on('click', panelSlideinClose)
  function panelSlideinClose () {
    $('.panel-slidein').removeClass('visible')
  }
})

function closeWithEsc () {
  $(document).keydown(function (e) {
    // if keypressed is ESC (keycode 27), hide panel-slidein
    if (e.keyCode === 27) {
      $('.panel-slidein').removeClass('visible')
    }
  })
}
