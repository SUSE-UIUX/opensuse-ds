var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  if (process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'production') {
    res.redirect('/dashboard')
  } else {
    res.render('icons/when-to-use-them')
  }
})

module.exports = router
