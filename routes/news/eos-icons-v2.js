var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('news/eos-icons-v2', {
    title: 'EOS icons version 2: Extended and animated | EOS',
    description: 'A new version of EOS icons that includes Material Icons, animated icons, and a whole load of new icons designed for open source products.',
    path: req.originalUrl
  })
})

module.exports = router
