var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('writing/ux-writing', {
    title: 'Writing guides: UX Writing | EOS',
    description: 'Our UX writing guide offers a set of general writing tips and best practices, which are very useful when writing content for any application.',
    path: req.originalUrl
  })
})

module.exports = router
