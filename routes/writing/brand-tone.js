var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('writing/brand-tone', {
    title: 'Writing guides: Brand Tone | EOS',
    description: 'A brand\'s Tone can change depending on the type of message you\'re trying to convey. It adapts to the emotional state of the person we\'re addressing.',
    path: req.originalUrl
  })
})

module.exports = router
