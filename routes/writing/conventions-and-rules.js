var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('writing/conventions-and-rules', {
    title: 'Writing guides: Conventions and Rules | EOS',
    description: 'This collection of writing conventions and rules helps developers implement a coherent content strategy.',
    path: req.originalUrl
  })
})

module.exports = router
