var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('writing/index', {
    title: 'Writing guides | EOS',
    description: 'EOS provides a comprehensive guide on how to communicate with users in a clear and consistent way, providing guidelines for Brand Voice, Brand Tone and UX Writing.',
    path: req.originalUrl
  })
})

module.exports = router
