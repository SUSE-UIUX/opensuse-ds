// these variables need to be defined in the global scope
// so renderIcons can access them instead of the ones defined in
// the controller
var $iconsContainer, iconDisplayTemplate
var getIconsCollectionService = sinon.fake()
var searchIconsService = sinon.fake()

describe("Eos icons", () => {

  describe('renderIcons', () => {
    var iconsWrap = $('<div class="js-eos-icons-list"><div class="icon-display"><div class="eos-icons"><strong class="icon-name">TEST</strong></div></div></div>')
    var collection = ["action_chains", "activate_subscriptions", "admin"]

    before((done) => {
      $('body').prepend(iconsWrap)
      $iconsContainer = $('.js-eos-icons-list')
      done()
    })

    after((done) => {
      $(iconsWrap).remove()
      done()
    })

    it('Should print the collection of 3 icons', () => {
      iconDisplayTemplate = $('.icon-display').clone(true)
      $('.icon-display').remove()
      renderIcons(collection)
      expect($('.icon-display')).to.have.lengthOf(3)
      expect($('.icon-display')).to.contain('action_chains')
      expect($('.icon-display')).to.contain('activate_subscriptions')
      expect($('.icon-display')).to.contain('admin')
    })
  })

  describe('On search', () => {

    it('Should reset the list of icons when the search is empty', () => {
      // create spies to track methods called
      var spy = sinon.spy(window, 'getFullIconsCollection')
      // make an empty search
      onSearch()
      expect(spy).called
    })

    it('Should filter the list of icons when the search is not empty', () => {
      // create spies to track methods called
      var spy = sinon.spy(window, 'getFilteredIconsCollection')
      // make an empty search
      onSearch('something')
      expect(spy).called
    })
  })

})
