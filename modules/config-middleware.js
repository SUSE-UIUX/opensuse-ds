var fs = require('fs')
var configData = JSON.parse(fs.readFileSync('./config.json'), 'utf8')

/* Initial middleware based on config file */
function projectConfig(req, res, next) {
  /* Load the parts we need from config object */
  var { showLanding, showChangelog, showFeedbackTool } = configData

  /* Remeber the path for future implementation */
  var requestRoute = req.originalUrl

  /* Send the object with res.locals so we can use the props inside our views. */
  res.locals.config = configData

  /* When the request is made, we check if the route corresponde with one of our defined routes, if they exist we create the locals for title and description. If we declare them outside this condition the API's would break */
  if (configData.routes[requestRoute]) {
    var { title, description } = configData.routes[requestRoute]

    res.locals.title = title
    res.locals.description = description
    res.locals.path = requestRoute
  }

  /* Handle redirection to dashboards for Landing & Changelog pages  */
  if ((requestRoute === '/' && showLanding === false) || (requestRoute === '/changelog' && showChangelog === false)) {
    res.redirect('/dashboard')
  } else {
    next()
  }
}

module.exports = projectConfig
